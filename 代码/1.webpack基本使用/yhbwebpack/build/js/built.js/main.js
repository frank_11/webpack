/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is not neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (function() { // webpackBootstrap
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
eval("/*\n  index.js: webpack入口起点文件\n\n  1. 运行指令：\n    开发环境：webpack ./src/index.js -o ./build/built.js --mode=development\n      webpack会以 ./src/index.js 为入口文件开始打包，打包后输出到 ./build/built.js\n      整体打包环境，是开发环境\n    生产环境：webpack ./src/index.js -o ./build/built.js --mode=production\n      webpack会以 ./src/index.js 为入口文件开始打包，打包后输出到 ./build/built.js\n      整体打包环境，是生产环境\n\n   2. 结论：\n    1. webpack能处理js/json资源，不能处理css/img等其他资源\n    2. 生产环境和开发环境将ES6模块化编译成浏览器能识别的模块化~\n    3. 生产环境比开发环境多一个压缩js代码。\n*/\n// import './index.css';\n\n\nfunction add(x, y) {\n  return x + y;\n}\n\nconsole.log(add(1, 2));\n\n\n//# sourceURL=webpack://yhbwebpack/./src/index.js?");
/******/ })()
;